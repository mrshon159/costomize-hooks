import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import useScroll from "../../hooks/useScroll";

const List = () => {

    const [todos, setTodos] = useState([])
    const [page, setPage] = useState(1)
    const limit = 20
    const refParent = useRef()
    const refChild = useRef()
    const intersection = useScroll(refParent, refChild, setTodos, () => fetchTodos(page, limit))
    function fetchTodos(page, limit) {
        return fetch(`https://jsonplaceholder.typicode.com/todos?_limit=${limit}&_page=${page}`)
            .then(response => response.json())
            .then(json => {
                setTodos(prev => [...prev, ...json])
                setPage(prev => prev + 1)
            })
    }


    return (
        <div ref={refParent} style={{height: '80vh', overflow: "auto"}}>
            {todos.map(todo => (
                <div key={todo.id} style={{padding: 30, border: '2px solid black '}}>
                    {todo.id} {todo.title}
                </div>
            ))}
            <div ref={refChild} style={{height: 30}}></div>
        </div>
    );
};

export default List;