import React, {useState} from 'react';
import useInput from "./hooks/useInput";
import Hover from "./components/Hover/Hover";
import List from "./components/List/List";
import useDebounce from "./hooks/useDebounce";
import useRequest from "./hooks/useRequest";
import axios from "axios";

const App = () => {

    // const username = useInput('')
    // const password = useInput('')

    // ===================================================
    // const [value, setValue] = useState('')
    //
    // const debounceSearch = useDebounce(search, 500)

    // function search(query) {
    //     return fetch(`https://jsonplaceholder.typicode.com/todos`)
    //         .then(response => response.json())
    //         .then(json => {
    //             console.log(json)
    //         })
    // }

    // const onChange = e => {
    //     setValue(e.target.value)
    //     debounceSearch(e.target.value)
    // =====================================================}
    //===========================================================
    // const [todos, loading, error] = useRequest(fetchTodos)
    //
    //
    // function fetchTodos(query) {
    //     return axios.get(`https://jsonplaceholder.typicode.com/todos`)
    // }
    //
    // if (loading) {
    //     return <h1>Идет загрузка.....</h1>
    // }
    //
    // if (error) {
    //     return <h1>Произошла ошибка загрузки данных</h1>
    // }
    //===========================================================

    return (
        <div>
            {todos && todos.map(todo => (
                <div key={todo.id} style={{padding: 30, border: '2px solid black '}}>
                    {todo.id} {todo.title}
                </div>
            ))}
            {/*<input type="text" value={value} onChange={onChange}/>*/}
            {/*<List />*/}
           {/*<Hover />*/}
          {/*<input {...username} type="text" placeholder="username" />*/}
          {/*<input {...password} type="text" placeholder="password" />*/}
          {/*<button onClick={() => console.log(username.value, password.value)}>Click</button>*/}
        </div>
    );
};

export default App;